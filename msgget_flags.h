#ifndef __MSGGET_FLAGS_H__
#define __MSGGET_FLAGS_H__
#include <Python.h>
#include "structmember.h"
#include <sys/ipc.h>

PyTypeObject MsggetFlagsType;

PyObject* Flags_tp_dict(void);

#endif