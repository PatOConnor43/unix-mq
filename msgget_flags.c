#include "msgget_flags.h"

typedef struct {
    PyObject_HEAD
} flags;

PyDoc_STRVAR(
    flags_new__doc__,
    "Constants used by msgget"
);


static PyMethodDef flags_Methods[]={
    {NULL}
};

static PyMemberDef flags_Members[]={
    {NULL}
};

static void flags_dealloc(flags* self) {
    Py_TYPE(self)->tp_free((PyObject *)self);
}


// /* flag definitions */
// IPC_CREAT  00001000   /* create if key is nonexistent */
// IPC_EXCL   00002000   /* fail if key exists */
// IPC_NOWAIT 00004000   /* return error on wait */
PyObject* Flags_tp_dict(void) {
    PyObject *flags_dict = PyDict_New();

    PyObject *IPC_CREAT_key = (PyObject*)PyUnicode_FromString("IPC_CREAT");
    PyObject *IPC_CREAT_value = (PyObject*)Py_BuildValue("i", IPC_CREAT);
    PyDict_SetItem(flags_dict, IPC_CREAT_key, IPC_CREAT_value);

    PyObject *IPC_EXCL_key = (PyObject*)PyUnicode_FromString("IPC_EXCL");
    PyObject *IPC_EXCL_value = (PyObject*)Py_BuildValue("i", IPC_EXCL);
    PyDict_SetItem(flags_dict, IPC_EXCL_key, IPC_EXCL_value);

    PyObject *IPC_NOWAIT_key = (PyObject*)PyUnicode_FromString("IPC_NOWAIT");
    PyObject *IPC_NOWAIT_value = (PyObject*)Py_BuildValue("i", IPC_NOWAIT);
    PyDict_SetItem(flags_dict, IPC_NOWAIT_key, IPC_NOWAIT_value);
    return flags_dict;
}

static int flags_init(flags *self, PyObject *args, PyObject *kwds) {

    return 0;
}



PyTypeObject MsggetFlagsType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "mq.MsggetFlags",
    sizeof(flags),
    0,
    (destructor)flags_dealloc,       /*tp_dealloc*/
    0,                  /*tp_print*/
    0,                  /*tp_getattr*/
    0,                  /*tp_setattr*/
    0,                  /*tp_reserved*/
    0,                  /*tp_repr*/
    0,                  /*tp_as_number*/
    0,                  /*tp_as_sequence*/
    0,                  /*tp_as_mapping*/
    0,                  /*tp_hash */
    0,                  /*tp_call */
    0,                  /*tp_str */
    0,                  /*tp_getattro */
    0,                  /*tp_setattro */
    0,                  /*tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags */
    flags_new__doc__,                  /*tp_doc */
    0,                  /*tp_traverse */
    0,                  /*tp_clear */
    0,                  /*tp_richcompare */
    0,                  /*tp_weaklistoffset */
    0,                  /*tp_iter */
    0,                  /*tp_iternext */
    flags_Methods,                  /*tp_methods */
    flags_Members,                  /*tp_members */
    0,                  /*tp_getset */
    0,                  /*tp_base */
    0,                  /*tp_dict */
    0,                  /*tp_descr_get */
    0,                  /*tp_descr_set */
    0,                  /*tp_dictoffset */
    (initproc)flags_init,                  /*tp_init */
    0,                  /*tp_alloc */
    0,                  /*tp_new */
};