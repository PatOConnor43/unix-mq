#ifndef __MQ_LIB_H__
#define __MQ_LIB_H__
#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "exceptions.h"

#define Mq_ftok_docs "Convert a pathname and a project identifier to a System V IPC key"
PyObject *Mq_ftok(PyObject *self, PyObject *args);
#define Mq_msgget_docs "Get a System V message queue identifier"
PyObject *Mq_msgget(PyObject *self, PyObject *args);
#define Mq_msgrcv_docs "Receive message from a System V message queue"
PyObject *Mq_msgrcv(PyObject *self, PyObject *args);


#endif