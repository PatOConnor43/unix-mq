#include "mq_lib.h"

#define debug_print(PYOBJECT) printf("DEBUG PRINT: %s\n", PyBytes_AS_STRING(PyUnicode_AsEncodedString(PyObject_Str(PYOBJECT), "utf-8", "~E~")));

PyObject *Mq_ftok(PyObject *self, PyObject *args) {
    char *pathname;
    int proj_id;

    if(!PyArg_ParseTuple(args, "si", &pathname, &proj_id)) {
        PyErr_SetString(FtokException, "Invalid parameters: Expected (str pathname, int proj_id)");
        return NULL;
    }
    int key = ftok(pathname, proj_id);
    if(key < 0) {
        PyErr_SetString(FtokException, strerror(errno));
        return NULL;
    }
    return Py_BuildValue("i", key);
}

PyObject *Mq_msgget(PyObject *self, PyObject *args) {
    int key;
    int msgflg;

    if(!PyArg_ParseTuple(args, "ii", &key, &msgflg)) {
        PyErr_SetString(MsggetException, "Invalid parameters: Expected (int key, int msgflg)");
        return NULL;
    }
    int id = msgget(key, msgflg);
    if (id < 0) {
        PyErr_SetString(MsggetException, strerror(errno));
        return NULL;
    }
    return Py_BuildValue("i", id);
}

PyObject *Mq_msgrcv(PyObject *self, PyObject *args) {
    int msgid;
    PyObject *rcv_class;
    long msgtyp;
    int msgflg;

    if (!PyArg_ParseTuple(args, "iOli", &msgid, &rcv_class, &msgtyp, &msgflg)) {
        PyErr_SetString(MsgrcvException, "Invalid parameters: Expected (int msgid, long msgtyp, int msgflg)");
        return NULL;

    }
    Py_INCREF(rcv_class);
    debug_print(rcv_class);
    Py_DECREF(rcv_class);
    char buffer[8*sizeof(char)*160*144+2];
    msgrcv(msgid, buffer, sizeof(buffer), 0, 0);
    int i;
    FILE *f = fopen("msg.hex", "w");
    for(i = 1; i < sizeof(buffer); i++) {
        fprintf(f, "0x%X\n", buffer[i]);
        // if(i % 8 == 159)
        //     fprintf(f, "\n"); 
    }
    // fprintf(f, "\n");
    fclose(f);
    return Py_BuildValue("i", 100);
}
