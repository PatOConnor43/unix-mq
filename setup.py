from setuptools import setup, Extension

setup(name="unix-mq",
      version="1.0.0",
      description="Python Extension Module for using Unix Message Queues",
      license="MIT",
      author="Patrick O'Connor",
      author_email="patoconnor43@gmail.com",
      url="http://gitlab.com/patoconnor43/unix-mq",
      ext_modules=[Extension("mq", [
            "mq_lib.c",
            "msgget_flags.c",
            "mq.c",
            ])],
      # test_suite="tests",
      )
