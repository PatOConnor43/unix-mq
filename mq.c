#include "mq.h"
#include "mq_lib.h"
#include "exceptions.h"
#include "msgget_flags.h"


PyMethodDef mq_funcs[] = {
    {
        "ftok",
        (PyCFunction)Mq_ftok,
        METH_VARARGS,
        Mq_ftok_docs
    },
    {
        "msgget",
        (PyCFunction)Mq_msgget,
        METH_VARARGS,
        Mq_msgget_docs
    },
    {
        "msgrcv",
        (PyCFunction)Mq_msgrcv,
        METH_VARARGS,
        Mq_msgrcv_docs
    }
};

char mqmod_docs[] = "mq";
char mqmod_name[] = "mq";

static struct PyModuleDef mq_mod = {
    PyModuleDef_HEAD_INIT,
    mqmod_name,
    mqmod_docs,
    -1,
    mq_funcs,
    NULL,
    NULL,
    NULL,
    NULL
};

PyMODINIT_FUNC PyInit_mq(void) {
    Py_Initialize();
    PyObject *m = PyModule_Create(&mq_mod);

    FtokException = PyErr_NewException("mq.FtokException", NULL, NULL);
    Py_INCREF(FtokException);
    PyModule_AddObject(m, "FtokException", FtokException);

    MsggetException = PyErr_NewException("mq.MsggetException", NULL, NULL);
    Py_INCREF(MsggetException);
    PyModule_AddObject(m, "MsggetException", MsggetException);

    MsgrcvException = PyErr_NewException("mq.MsgrcvException", NULL, NULL);
    Py_INCREF(MsgrcvException);
    PyModule_AddObject(m, "MsgrcvException", MsgrcvException);

    if (PyType_Ready(&MsggetFlagsType)<0) {
        printf("MsggetFlags not ready\n");
        return m;
    }
    MsggetFlagsType.tp_dict = Flags_tp_dict();

    Py_IncRef((PyObject*)&MsggetFlagsType);
    PyModule_AddObject(m, "MsggetFlags", (PyObject*)&MsggetFlagsType);

    return m;
}

