import mq

class MessagePayload:
    def __init__(self):
        self.a = ''

print(dir(mq))
print(dir(mq.MsggetFlags))
print(dir(mq.MsggetFlags.IPC_CREAT))
print(mq.MsggetFlags.IPC_CREAT)
# print(mq.Flags.IPC_CREAT)
msgkey = mq.ftok('/tmp/msgqueue', 65)
msgid = mq.msgget(msgkey, 0o666 | mq.MsggetFlags.IPC_CREAT)
result = mq.msgrcv(msgid, MessagePayload, 1, 0)
print(result)