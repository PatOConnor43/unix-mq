all: clean
	python setup.py build_ext --inplace
	python test.py

clean:
	rm -rf *.out *.bin *.exe *.o *.a *.so test build
